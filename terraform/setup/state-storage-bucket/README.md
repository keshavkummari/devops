### Setting up your state backend with S3

#### Types of state-storage
https://www.terraform.io/docs/backends/index.html
https://www.terraform.io/docs/backends/types/s3.html

#### Plan command and parameters
https://www.terraform.io/docs/commands/plan.html

#### Procedure
Run manually for the first time from your devstation for the plan:
`terraform plan -out=/vagrant/example-project/terraform/setup/state-storage-bucket/state/setup_state_storage.plan /vagrant/example-project/terraform/setup/state-storage-bucket`
You can override variables with -var. For example:
`terraform plan -out=setup_state_storage.plan -var 'env=example' /vagrant/example-project/terraform/setup/state-storage-bucket`

And for the apply:
`terraform apply /vagrant/example-project/terraform/setup/state-storage-bucket/state/setup_state_storage.plan`


#### Outputs

Outputs:

state-kms-arn = arn:aws:kms:eu-west-1:424506183973:key/dd024b27-9b1d-4560-8c04-233b209f9156

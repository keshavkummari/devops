# We need the key for the s3 encryption
output "state-kms-arn" {
  value = "${aws_kms_alias.state.arn}"
}

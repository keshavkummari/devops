resource "aws_security_group" "prs_elb" {
  name   = "PRS Load Balancer"
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name  = "PRS-SG-elb"
    Owner = "Rene.Schoonrok@nn-group.com"
  }
}

resource "aws_security_group" "prs_app" {
  name   = "PRS Application"
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name  = "PRS-SG-app"
    Owner = "Rene.Schoonrok@nn-group.com"
  }
}

resource "aws_security_group" "prs_db" {
  name   = "PRS Database"
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name  = "PRS-SG-db"
    Owner = "Rene.Schoonrok@nn-group.com"
  }
}

resource "aws_security_group_rule" "elb_ingress_https" {
  security_group_id = "${aws_security_group.prs_elb.id}"

  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "elb_egress_app" {
  security_group_id = "${aws_security_group.prs_elb.id}"

  type                     = "egress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_app.id}"
}

resource "aws_security_group_rule" "app_ingress_elb" {
  security_group_id = "${aws_security_group.prs_app.id}"

  type                     = "ingress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_elb.id}"
}

resource "aws_security_group_rule" "app_egress_db" {
  security_group_id = "${aws_security_group.prs_app.id}"

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "db_ingress_app" {
  security_group_id = "${aws_security_group.prs_db.id}"

  type                     = "ingress"
  from_port                = 1521
  to_port                  = 1521
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_app.id}"
}

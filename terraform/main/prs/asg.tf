data "aws_ami" "hof_prs_ami" {
  most_recent = true

  #  filter {
  #    name   = "hof_prs_ami_rsk"
  #    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  #  }


  #  filter {
  #    name   = "virtualization-type"
  #    values = ["hvm"]
  #  }

  owners = ["424506183973"] # Canonical
}

resource "aws_launch_configuration" "as_conf" {
  name_prefix          = "hof_prs_app"
  security_groups      = ["${aws_security_group.prs_app.id}"]
  iam_instance_profile = "arn:aws:iam::424506183973:instance-profile/tf-glrunner-b6e0a732b-master"

  image_id      = "${var.pckr_ami_id}"
  instance_type = "t2.micro"
  key_name      = "hof_dev"

  #fill environment file with STAGE /etc/environment
  #  user_data = <<EOF
  ##!/bin/bash
  #echo "STAGE=${var.env}" > /etc/environment
  #EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "hof_prs_asg" {
  #name_prefix         = "hof_prs_asg"
  name                 = "hof_prs_asg-${var.env}"
  launch_configuration = "${aws_launch_configuration.as_conf.name}"

  vpc_zone_identifier = ["${data.aws_subnet_ids.trusted.ids}"]

  #vpc_zone_identifier = ["${var.subnet_platform_trusted_az_a}"]
  load_balancers = ["${aws_elb.hof_prs_elb.name}"]
  min_size       = 1
  max_size       = 1

  lifecycle {
    create_before_destroy = true
  }
}

# Create a new load balancer

resource "aws_elb" "hof_prs_elb" {
  name = "hof-prs-elb"

  # availability_zones = ["eu-west-1a"]

  subnets         = ["${data.aws_subnet_ids.trusted.ids}"]
  security_groups = ["${aws_security_group.prs_elb.id}"]
  #subnets         = ["${var.subnet_platform_trusted_az_a}"]
  access_logs {
    bucket        = "hof-dev-bucket"
    bucket_prefix = "elb_logs"
    interval      = 60
  }
  internal = true
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTPS:8443/"
    interval            = 30
  }
  listener {
    instance_port      = 8080                                                                                  # backing instance port, so outgoing
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "arn:aws:acm:eu-west-1:424506183973:certificate/3e780fd9-b138-483f-aaa5-605cdd35eaf9"
  }
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  tags {
    Name  = "HOF-PRS-elb"
    Owner = "Rene.Schoonrok@nn-group.com"
  }
}

############################################
# ENVIRONMENT (ACCOUNT) SPECIFIC VARIABLES #
############################################

# Which env
env = "dev"

#Specify the statebucket
statebucket = "tf-state-storage-hof-prs-app-${var.env}"

# Specify the owner of this account
owner = "424506183973"

# Specify the forward proxy security group id for this environment
#sg_proxy = "sg-4b77a232"

# Specify domain where to create DNS record
#dns_domain = "aws.insim.biz"

######################
# INSTANCE VARIABLES #
######################

# Specify the size of EC2 instances for this environment
instance_type = "t2.micro"

# Specify the default EC2 instance role  for this environment
instance_iam_profile = "arn:aws:iam::424506183973:instance-profile/secaec2instanceprofile"

instance_wait_elb = "1"

instance_min = "1"

instance_max = "1"

####################
# ACCESS FILTERING #
####################

# Specify IP ranges in CIDR format to define who can access the application
elb_src_cidrs = ["10.0.0.0/8", "10.1.2.3/32"]

############################################
# ENVIRONMENT (ACCOUNT) SPECIFIC VARIABLES #
############################################

variable "pckr_ami_id" {
  description = "packer ami id"
  type        = "string"
  default     = "ami-5aea7d23"
}

variable "statebucket" {
  description = "Account statebucket for the example-app"
  type        = "string"
}

variable "owner" {
  description = "Owner of this AWS account"
  type        = "string"
  default     = "424506183973"
}

variable "env" {
  description = "Name of the environment"
  type        = "string"
}

######################
# INSTANCE VARIABLES #
######################

variable "instance_type" {
  description = "Instance size"
  type        = "string"
  default     = "t2.micro"
}

variable "instance_iam_profile" {
  description = "Instance profile arn"
  type        = "string"
  default     = "arn:aws:iam::424506183973:instance-profile/tf-glrunner-b6e0a732b-master"
}

# 0 = to turned off, 1 is turned on  # Terraform does not support boolean, but convert false to 0 and true to 1. Using count parameter at the resource, you can switch resource on and off.
# For more info refer to https://www.terraform.io/docs/configuration/variables.html
variable "schedule_nightly_shutdown" {
  description = "Turns on or off nightly shutdown to save costs"
  default     = false
}

####################
# ACCESS FILTERING #
####################

#variable "elb_src_cidrs" {
#  description = "IP ranges in CIDR format to define who can access the application"
#  type        = "list"
#}

####################
# KMS Variables    #
####################

variable kms-usage-arns {
  description = "list of role arns that will have access to s3 and kms"
  type        = "list"
  default     = ["arn:aws:iam::424506183973:role/tf-glrunner-b6e0a732b-master", "arn:aws:iam::424506183973:role/Admin", "arn:aws:iam::424506183973:role/Engineer"]
}

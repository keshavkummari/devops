data "aws_vpc" "selected" {
  tags {
    Owner = "aws-team"
  }
}

data "aws_subnet_ids" "trusted" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Zone = "trusted"
    Name = "tf-platform-trusted-eu-west-1a"
  }
}
